﻿using Lab6.ControlPanel.Contract;
using PK.Container;
using System;
using System.Reflection;



namespace Lab6.Infrastructure
{
    public struct LabDescriptor
    {
        #region P1

        public static Func<IContainer> ContainerFactory = () => new Kontener.Container();

        public static Assembly ControlPanelSpec = Assembly.GetAssembly(typeof(IControlPanel));
        public static Assembly ControlPanelImpl = Assembly.GetAssembly(typeof(ControlPanel.Implementation.ControlPanelObject));

        public static Assembly MainComponentSpec = Assembly.GetAssembly(typeof(MainComponent.Contract.IMainComponent));
        public static Assembly MainComponentImpl = Assembly.GetAssembly(typeof(MainComponent.Implementation.MainComponentObject));

        #endregion
    }
}
