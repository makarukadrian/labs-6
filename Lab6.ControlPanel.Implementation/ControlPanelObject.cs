﻿using Lab6.ControlPanel.Contract;
using Lab6.MainComponent.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Lab6.ControlPanel.Implementation
{
    public class ControlPanelObject : IControlPanel
    {
        private IMainComponent _mainComponent;

        public ControlPanelObject(IMainComponent mainComponent)
        {
            _mainComponent = mainComponent;
        }

        public System.Windows.Window Window
        {
            get
            {
                return new Window();
            }
        }
    }
}
