﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PK.Container;
using System.Reflection;

namespace Kontener
{
    public class Container : IContainer
    {
        //Dla każdego typu interfejsu przypisujemy typ klasy, która dziedziczy po nim.
        private Dictionary<Type, Type> KlasyIINterfejsy = new Dictionary<Type, Type>();
        //Jest tutaj zapisany każdy interfejs, który posiada utworzony obiekt zgodny z jego typem.
        private Dictionary<Type, object> InterfejsyIInstancje = new Dictionary<Type, object>();

        public void Register<T>(Func<T> provider) where T : class
        {
            T obiekt = provider.Invoke();
            Register<T>(obiekt);
        }
        public void Register<T>(T impl) where T : class
        {
            Type[] interfejsy = impl.GetType().GetInterfaces();
            foreach (Type interfejs in interfejsy)
            {
                if (!InterfejsyIInstancje.ContainsKey(interfejs))
                {
                    InterfejsyIInstancje.Add(interfejs, impl);
                }
                else
                {
                    InterfejsyIInstancje[interfejs] = impl;
                }
            }
        }
        public void Register(Type type)
        {
            Type[] localTypes = type.GetInterfaces();

            foreach (Type singleType in localTypes)
            {
                if (KlasyIINterfejsy.ContainsKey(singleType))
                {
                    KlasyIINterfejsy[singleType] = type;
                }

                else
                {
                    KlasyIINterfejsy.Add(singleType, type);
                }
            }
        }
        public void Register(Assembly assembly)
        {
            Type[] localTypes;
            localTypes = assembly.GetTypes();

            foreach (Type singleType in localTypes)
            {
                if (!singleType.IsNotPublic)
                {
                    this.Register(singleType);
                }
            }
        }
        public object Resolve(Type type)
        {
            if (InterfejsyIInstancje.ContainsKey(type))
            {
                return InterfejsyIInstancje[type];
            }

            else if (!KlasyIINterfejsy.ContainsKey(type))
            {
                return null;
            }

            else
            {
                ConstructorInfo[] konstruktory = KlasyIINterfejsy[type].GetConstructors();

                foreach (ConstructorInfo item in konstruktory)
                {
                    ParameterInfo[] info = item.GetParameters();

                    if (info.Length == 0)
                    {
                        return Activator.CreateInstance(KlasyIINterfejsy[type]);
                    }

                    List<object> lista = new List<object>(info.Length);

                    foreach (ParameterInfo pinfo in info)
                    {
                        var p = Resolve(pinfo.ParameterType);

                        if (p == null)
                        {
                            throw new UnresolvedDependenciesException();
                        }

                        lista.Add(p);
                    }

                    return item.Invoke(lista.ToArray());
                }
            }

            return null;
        }
        public T Resolve<T>() where T : class
        {
            return (T)Resolve(typeof(T));
        }
    }
}
